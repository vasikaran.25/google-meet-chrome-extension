# Google Meet Chrome Extension

This chrome extension allows users to generate a Google Meet link instantly and they able to share it with social platforms

# Features
* Create a new Google Meet link
* Share the meeting link with social mediums

# Installation

Clone Repo
```
    git clone https://gitlab.com/vasikaran.25/google-meet-chrome-extension.git
```

Go to `google-meet-chrome-extension` directory and run
```
npm install
```

Now build the extension using
```
npm run start
```

# Adding this extension to Chrome

Please refer [this link](https://developer.chrome.com/docs/extensions/mv3/getstarted/#unpacked) for adding this extension to Chrome

# Developer responsibilites 

Since it is coupled with my oAuth2 client, I need your application id for Google oAuth2 authentication. I want to update this on my developer console. 
FYI, You can use your oAuth2 client if you want

![Application Id](https://i.ibb.co/XfH0tD5/Screenshot-2022-03-02-at-12-37-31-PM.png)

# Incompleted works
* Unit Test case
* Components spliting for more reusable 

# Conclusion
I hope I did well. Here is the [demo video](https://drive.google.com/file/d/1LMT-jIU_e8vPrs93oxjfykWcr_bZtupn/view?usp=sharing)
