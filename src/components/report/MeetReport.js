import React, { useState } from "react";
import { Box, Stack, Chip, Snackbar } from "@mui/material";
import ContentCopy from "@mui/icons-material/ContentCopy";
import CloseIcon from "@mui/icons-material/Close";

import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
  WhatsappShareButton,
  WhatsappIcon,
} from "react-share";

import "./MeetReport.css";

const MeetReport = (props) => {
  let { meetLink, email } = props;

  let [isCopied, setIsCopied] = useState(false);

  let copyToClipboard = () => {
    navigator.clipboard.writeText(meetLink).then(() => {
      setIsCopied(true);
    });
  };

  let handleAlertClose = () => {
    setIsCopied(false);
  };

  return (
    <Stack alignItems={"center"}>
      <Box
        sx={{
          padding: "50px",
          fontSize: "20px",
          paddingBottom: "25px",
        }}
      >
        Your Meeting link is ready !!
      </Box>
      <Box>
        <Chip
          label={meetLink}
          deleteIcon={<ContentCopy />}
          onDelete={copyToClipboard}
        ></Chip>
      </Box>
      <Stack alignItems={"center"} sx={{ marginTop: "70px" }}>
        <Box
          sx={{
            padding: "15px",
            fontSize: "16px",
          }}
        >
          Share with
        </Box>
        <Box>
          <FacebookShareButton url={meetLink}>
            <FacebookIcon size={32} round={true} />
          </FacebookShareButton>
          <TwitterShareButton url={meetLink}>
            <TwitterIcon size={32} round />
          </TwitterShareButton>
          <WhatsappShareButton url={meetLink}>
            <WhatsappIcon size={32} round />
          </WhatsappShareButton>
        </Box>
      </Stack>
      <Snackbar
        open={isCopied}
        autoHideDuration={3000}
        onClose={handleAlertClose}
        message="Copied meeting link"
        action={<CloseIcon fontSize="small" onClick={handleAlertClose} />}
      ></Snackbar>
    </Stack>
  );
};

export default MeetReport;
