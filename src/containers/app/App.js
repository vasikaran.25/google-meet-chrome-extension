import React, { useState, useEffect } from "react";
import { Container, Box } from "@mui/material";
import { styled } from "@mui/material/styles";

import Login from "../login/Login";
import MeetContainer from "../meet/MeetContainer";

const CustomContainer = styled(Container)({
  paddingLeft: "0px",
  paddingRight: "0px",
});

const App = () => {
  let [token, setToken] = useState(null);

  useEffect(() => {
    chrome.storage.local.get(["token"], ({ token }) => {
      if (token) {
        chrome.identity.getAuthToken({ interactive: true }, (token) => {
          if (token) {
            setToken(token);
            chrome.storage.local.set({ token });
          }
        });
      }
    });
  });

  const handleLogin = () => {
    chrome.storage.local.get(["token"], ({ token }) => {
      if (!token) {
        chrome.identity.getAuthToken({ interactive: true }, (token) => {
          if (token) {
            setToken(token);
            chrome.storage.local.set({ token });
          }
        });
      }
    });
  };

  const handleLogout = () => {
    fetch(`https://oauth2.googleapis.com/revoke?token=${token}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
      .then(() => {
        chrome.identity.clearAllCachedAuthTokens(() => {
          chrome.storage.local.remove(["token"]);
          setToken(null);
        });
      })
      .catch(() => {
        chrome.identity.clearAllCachedAuthTokens(() => {
          chrome.storage.local.remove(["token"]);
          setToken(null);
        });
      });
  };

  return (
    <React.Fragment>
      <CustomContainer fixed>
        <Box
          sx={{
            bgcolor: "#fff",
            height: "400px",
            width: "400px",
          }}
        >
          {!token && <Login handleLogin={handleLogin} />}
          {token && <MeetContainer onLogout={handleLogout} token={token} />}
        </Box>
      </CustomContainer>
    </React.Fragment>
  );
};

export default App;
