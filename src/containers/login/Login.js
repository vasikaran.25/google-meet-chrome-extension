import React from "react";
import { Paper, Button, Box, Stack } from "@mui/material";
import { styled } from "@mui/material/styles";
import CloseIcon from "@mui/icons-material/Close";

import GMeetLoader from "../../assets/gmeet-loader.svg";
import GoogleIcon from "../../assets/google.svg";
import "./Login.css";

const Img = styled("img")({
  margin: "auto",
  display: "block",
  maxWidth: "100%",
  maxHeight: "100%",
});

const Icon = styled("img")({
  margin: "auto",
  display: "block",
  maxWidth: "25px",
  maxHeight: "100%",
});

const ItemEnd = styled(Box)({
  textAlign: "end",
  margin: "10px",
});

const Item = styled(Box)({
  textAlign: "center",
});

const Item1 = styled(Box)({
  textAlign: "center",
  boxShadow: "0px 0px 4px 1px grey",
  margin: "16px 10px 0px 10px !important",
});

const GoogleButton = styled(Button)({
  backgroundColor: "#000",
  textTransform: "none",
  padding: "10px 90px",
  marginTop: "50px",
  fontSize: "16px",
  "&:hover": {
    backgroundColor: "#fff",
    color: "#000",
  },
});

const CustomPaper = styled(Paper)({
  height: "100%",
});

const Login = (props) => {
  let { handleLogin } = props;
  return (
    <CustomPaper elevation={3}>
      <Stack spacing={2} justifyContent="space-evenly">
        <ItemEnd>
          <CloseIcon
            onClick={() => {
              window.close();
            }}
          />
        </ItemEnd>
        <Item
          sx={{
            fontWeight: "bold",
            fontSize: "20px",
          }}
        >
          Create and share a Google Meet Link
        </Item>
        <Item1>
          <Img src={GMeetLoader}></Img>
        </Item1>
        <Item>
          <GoogleButton
            variant="contained"
            startIcon={<Icon src={GoogleIcon} />}
            onClick={handleLogin}
          >
            Continue with Google
          </GoogleButton>
        </Item>
      </Stack>
    </CustomPaper>
  );
};

export default Login;
