import React, { Component } from "react";
import TextField from "@mui/material/TextField";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import MobileTimePicker from "@mui/lab/MobileTimePicker";
import DateAdapter from "@mui/lab/AdapterMoment";
import { Paper, Box, Stack, Avatar, Button } from "@mui/material";
import VideoCallIcon from "@mui/icons-material/VideoCall";
import { styled } from "@mui/material/styles";
import LoadingButton from "@mui/lab/LoadingButton";

import MeetReport from "../../components/report/MeetReport";
import "./Meet.css";

const Item = styled(Box)((props) => {
  let { padding, marginLeft = 0, color = "initial", cursor } = props;
  let styles = {
    padding: `${padding}px`,
    marginLeft: `${marginLeft}px !important`,
    color,
  };

  if (cursor) {
    styles.cursor = cursor;
  }

  return styles;
});

const ProfileAvatar = styled(Avatar)({
  height: "35px",
  width: "35px",
  borderRadius: "5px",
});

const CustomPaper = styled(Paper)({});

class MeetContainer extends Component {
  constructor() {
    super();
    this.handleCreateMeeting = this.handleCreateMeeting.bind(this);
    this.setTimeValue = this.setTimeValue.bind(this);
    this.resetMeetLink = this.resetMeetLink.bind(this);
  }

  state = {
    meetLink: null,
    time: {
      start: null,
      end: null,
    },
  };

  handleCreateMeeting(currentTime) {
    let { token } = this.props;
    let body = {
      end: {
        dateTime: this.state.time.end || currentTime,
      },
      start: {
        dateTime: this.state.time.start || currentTime,
      },
      description: "Sample one from extension",
      conferenceData: {
        createRequest: {
          conferenceSolutionKey: {
            type: "hangoutsMeet",
          },
          requestId: "new-meet",
        },
      },
    };
    let init = {
      method: "POST",
      async: true,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    };
    fetch(
      "https://www.googleapis.com/calendar/v3/calendars/primary/events?conferenceDataVersion=1&key=AIzaSyDFvU8Lc5wf9ZW1KPuNVyDIEN0_wmX--8g",
      init,
    )
      .then((response) => response.json())
      .then((data) => {
        let { hangoutLink: meetLink } = data;
        this.setState({
          meetLink,
          isLoading: false,
          isMeetingCreated: true,
        });
      });
    this.setState({
      isLoading: true,
    });
  }

  setTimeValue(type, val) {
    let { time } = this.state;
    time[type] = val;
    this.setState({ time });
  }

  resetMeetLink() {
    this.setState({
      meetLink: null,
    });
  }

  render() {
    let { isLoading, meetLink, user = {} } = this.state;
    let { onLogout } = this.props;
    const currentTime = new Date().toISOString();
    let {
      time: { start, end },
    } = this.state;
    return (
      <React.Fragment>
        <CustomPaper
          elevation={3}
          sx={{
            padding: "5px",
            fontWeight: "bold",
            fontSize: "16px",
          }}
        >
          <Stack
            spacing={2}
            direction="row"
            alignItems={"center"}
            justifyContent={"space-between"}
          >
            <Stack spacing={2} direction="row" alignItems={"center"}>
              <Item padding={"10"} marginLeft={"12"}>
                <ProfileAvatar
                  variant="square"
                  src={user ? user.picture : null}
                ></ProfileAvatar>
              </Item>
              <Item name="Vasi" padding={"5"}>
                My workspace
              </Item>
            </Stack>
            {meetLink && (
              <Button
                variant="text"
                startIcon={<VideoCallIcon />}
                sx={{
                  textTransform: "none",
                }}
                onClick={this.resetMeetLink}
              >
                Create
              </Button>
            )}
            <Item
              onClick={onLogout}
              padding={"10"}
              color={"#ff0000"}
              cursor={"pointer"}
            >
              Logout
            </Item>
          </Stack>
        </CustomPaper>
        {meetLink && (
          <MeetReport meetLink={meetLink} email={user.email}></MeetReport>
        )}
        {!meetLink && (
          <LocalizationProvider dateAdapter={DateAdapter}>
            <Stack
              spacing={2}
              direction="row"
              sx={{
                marginTop: "40px",
              }}
              justifyContent={"space-around"}
            >
              <MobileTimePicker
                label="Start Time"
                onChange={(val) => {
                  this.setTimeValue("start", val);
                }}
                renderInput={(params) => (
                  <TextField
                    className={"input"}
                    {...params}
                    sx={{ width: "150px" }}
                  />
                )}
                value={start || currentTime}
                onChange={(val) => {
                  this.setTimeValue("start", val);
                }}
              />
              <MobileTimePicker
                label="End Time"
                renderInput={(params) => (
                  <TextField
                    className={"input"}
                    {...params}
                    sx={{ width: "150px" }}
                  />
                )}
                value={end || currentTime}
                onChange={(val) => {
                  this.setTimeValue("end", val);
                }}
              />
            </Stack>
          </LocalizationProvider>
        )}
        {!meetLink && (
          <Stack
            sx={{
              padding: "35px 25px",
            }}
          >
            <LoadingButton
              variant="contained"
              startIcon={<VideoCallIcon />}
              sx={{
                textTransform: "none",
                padding: "12px",
                bgcolor: true ? "#1976d2" : "initial",
              }}
              loading={isLoading}
              onClick={() => {
                this.handleCreateMeeting(currentTime);
              }}
            >
              Create New Meeting
            </LoadingButton>
          </Stack>
        )}
      </React.Fragment>
    );
  }

  componentDidMount() {
    let fetcher = (url) => {
      return fetch(url, {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }).then((res) => {
        return res.json();
      });
    };
    let { token } = this.props;
    fetcher("https://www.googleapis.com/oauth2/v3/userinfo", token).then(
      (user) => {
        this.setState({
          user,
        });
      },
    );
  }
}

export default MeetContainer;
