// Do this as the first thing so that any code reading it knows the right env.
process.env.BABEL_ENV = "development";
process.env.NODE_ENV = "development";

var WebpackDevServer = require("webpack-dev-server"),
  webpack = require("webpack"),
  config = require("./webpack.config"),
  path = require("path");

for (var entryName in config.entry) {
  config.entry[entryName] = [
    "webpack/hot/dev-server",
    `webpack-dev-server/client?hot=true&hostname=localhost&port=9090`,
  ].concat(config.entry[entryName]);
}

config.plugins = [new webpack.HotModuleReplacementPlugin()].concat(
  config.plugins || [],
);

var compiler = webpack(config);

var server = new WebpackDevServer(
  {
    https: false,
    hot: false,
    client: false,
    host: "localhost",
    port: 9090,
    static: {
      directory: path.join(__dirname, "../build"),
    },
    devMiddleware: {
      publicPath: `http://localhost:9090/`,
      writeToDisk: true,
    },
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
    allowedHosts: "all",
  },
  compiler,
);

if (process.env.NODE_ENV === "development" && module.hot) {
  module.hot.accept();
}

(async () => {
  await server.start();
  console.log("http://localhost:9090/");
})();
